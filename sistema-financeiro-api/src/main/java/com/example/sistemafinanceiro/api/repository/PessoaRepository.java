package com.example.sistemafinanceiro.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.sistemafinanceiro.api.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long>{

}
