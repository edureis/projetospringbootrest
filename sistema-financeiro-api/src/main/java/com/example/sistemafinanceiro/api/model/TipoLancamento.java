package com.example.sistemafinanceiro.api.model;

public enum TipoLancamento {
	
	RECEITA,
	DESPESA
}
