package com.example.sistemafinanceiro.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.sistemafinanceiro.api.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long>{
	
	
}
